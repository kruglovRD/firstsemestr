using HorizonSideRobots
include("roblib.jl")

" Помечает маркером всё пустое >ПРЯМОУГОЛЬНОЕ< поле и возвращается в первоначальную позицию "
function mark_field(r)
    " Передвижение в нижний правый угол и получение пути перемещения "
    way = move_to_corner(r)

    " Маркирование каждого ряда от крайней левой стены, до крайней правой и обратно "
    " Note: может врезаться в стену, если поле НЕ прямоугольное (например, стоит одна стена в противоположной стороне от той, в которой проверялся статус наличия стены) "
    side = left
    while !(isborder(r, up)) || !(isborder(r, side))
        putmarker!(r)
        mark_up_to_border(r, side)
        if !(isborder(r, up))
            move!(r, up)
            side = inverse(side)
        end
    end
    
    " Вовзращение в первоначальную позицию "
    returnToTheBase(r, way)
end
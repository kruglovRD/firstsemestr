using HorizonSideRobots
include("roblib.jl")

" Маркирование поля косым крестом (в форме X), в центре которого находится первоначальная позиция исполнителя "
function result(r)
    for i in 0:3
        " Маркирование пути от центра "
        while !isborder(r, HorizonSide(i)) && !isborder(r, HorizonSide((i+1)%4))
            move!(r, HorizonSide(i))
            move!(r, HorizonSide((i+1)%4))
            putmarker!(r)
        end
        " Поиск первоначальной позиции исполнителя по замаркированному пути "
        while ismarker(r)
            move!(r, inverse(HorizonSide(i)))
            move!(r, inverse(HorizonSide((i+1)%4)))
        end
    end
    putmarker!(r)
end
using HorizonSideRobots
include("roblib.jl")

" Находит внутреннюю перегородку в форме прямоугольника и расставляет по его периметру маркеры "
function mark_perimeter_partition(r::Robot)
    " Поиск препятствия в поле "
    find_partition(r)

    " Определяет сторону, в зависимости от которой определяется порядок обхода перегородки прямоугольной формы "
    if isborder(r,left)
        for i in 0:3
            " Проходит вдоль перегородки, проставляя маркеры, затем становится у следующей стороны перегородки "
            mark_along_the_border(r, HorizonSide(mod(i+1, 4)), HorizonSide(i))
            move!(r, HorizonSide(mod(i+1, 4)))
        end
    else
        for i in 0:3
            " Проходит вдоль перегородки, проставляя маркеры, затем становится у следующей стороны перегородки "
            mark_along_the_border(r, HorizonSide(mod(i+3, 4)), HorizonSide(mode(i+2, 4)))
            move!(r, HorizonSide(mod(i+3, 4)))
        end
    end
end
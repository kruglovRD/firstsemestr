using HorizonSideRobots
include("roblib.jl")

" Помечает периметр внешней рамки маркером и возвращается в первоначальную позицию "
function mark_perimeter(r::Robot)
    " Передвижение в нижний правый угол и получение пути перемещения "
    way = move_to_corner(r)

    " Обход и попутное маркирование периметра "
    for i in 0:3 
        mark_up_to_frame(r, HorizonSide(i))
    end

    " Перемещение из правого нижнего угла в первоначальную позицию по обратному пути "
    returnToTheBase(r, way)
end

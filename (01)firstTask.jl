using HorizonSideRobots
include("roblib.jl")

" Помечает своё местоположение маркером, отмечая путь до ближайших стен по осям x и y и своего первоначального положения "
function mark_cross(r::Robot)
    " Обход по всем направлениям, отмечая путь маркером "
    for i in 0:3
        " Передвижение до стены, отмечая путь маркером "
        mark_up_to_border(r, HorizonSide(i))
        
        " Перемещение по клеткам, отмеченными маркером "
        move_to_marker(r,inverse(HorizonSide(i)))
    end

    putmarker!(r)
end
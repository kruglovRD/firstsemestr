up= HorizonSide(0)
left = HorizonSide(1)
down = HorizonSide(2)
right = HorizonSide(3) 


" Помечание пути исполнителя до стены маркером "
function mark_up_to_border(r::Robot, side::HorizonSide)
    while !(isborder(r, side))
        move!(r, side)
        putmarker!(r)
    end
end

" Маркировка прямого пути 'насквозь' до внешней рамки "
function mark_up_to_frame(r::Robot, side::HorizonSide)
    flag = true
    while flag
        " Пока есть возможность передвижения в заданном направлении "
        while !(isborder(r, side))
            move!(r, side)
            putmarker!(r)
        end
        " k - счётчик передвижений вбок"
        k = 0
        " Попытка найти конец перегородки, находящейся впереди "
        while isborder(r, side) && (!isborder(r, HorizonSide((Int(side)+1)%4)))
            move!(r, HorizonSide((Int(side)+1)%4))
            k += 1
        end
        " Если удалось найти конец перегородки - встаёт сбоку от неё "
        if !isborder(r, side)
            move!(r,side)
        end
        " Если препятствием была внешняя рамка - начать прекращение работы цикла функции "
        if isborder(r,side) && isborder(r,HorizonSide((Int(side)+1)%4))
            flag = false
        end
        " Пока сбоку прямоугольная перегородка, которая мешает передвижению - идти вдоль неё, пока она не закончится "
        while isborder(r,HorizonSide((Int(side)+3)%4))
            move!(r, side)
        end
        " Вернуть в первоначальную траекторию передвижения "
        while k > 0 
            move!(r, HorizonSide((Int(side)+3)%4))
            k -= 1
        end
        putmarker!(r)
    end
end

" Передвижение по прямому пути 'насквозь' до внешней рамки"
function move_up_to_frame(r::Robot, side::HorizonSide)
    flag = true
    while flag
        " Пока есть возможность передвижения в заданном направлении "
        while !(isborder(r, side))
            move!(r, side)
        end
        " k - счётчик передвижений вбок"
        k = 0
        " Попытка найти конец перегородки, находящейся впереди "
        while isborder(r, side) && (!isborder(r, HorizonSide((Int(side)+1)%4)))
            move!(r, HorizonSide((Int(side)+1)%4))
            k += 1
        end
        " Если удалось найти конец перегородки - встаёт сбоку от неё "
        if !isborder(r, side)
            move!(r,side)
        end
        " Если препятствием была внешняя рамка - начать прекращение работы цикла функции "
        if isborder(r,side) && isborder(r,HorizonSide((Int(side)+1)%4))
            flag = false
        end
        " Пока сбоку прямоугольная перегородка, которая мешает передвижению - идти вдоль неё, пока она не закончится "
        while isborder(r,HorizonSide((Int(side)+3)%4))
            move!(r, side)
        end
        " Вернуть в первоначальную траекторию передвижения "
        while k > 0 
            move!(r, HorizonSide((Int(side)+3)%4))
            k -= 1
        end
    end
end

" Передвижение по прямому пути 'насквозь' до клетки, которая не замаркированна "
function move_up_to_noMarkCell(r::Robot, side::HorizonSide)
    flag = true
    while flag
        " Пока есть возможность передвижения в заданном направлении и работает необходимый флаг "
        while !(isborder(r, side)) && flag
            move!(r, side)
            if !ismarker(r)
                flag = false
            end
        end
        " k - счётчик передвижений вбок"
        k = 0
        " Попытка найти конец перегородки, находящейся впереди "
        while isborder(r, side) && (!isborder(r, HorizonSide((Int(side)+1)%4)))
            move!(r, HorizonSide((Int(side)+1)%4))
            k += 1
        end
        " Если удалось найти конец перегородки - встаёт сбоку от неё, при условии работающего флага "
        if !isborder(r, side) && flag
            move!(r,side)
        end
        " Пока работает флаг и сбоку прямоугольная перегородка, которая мешает передвижению - идти вдоль неё, пока она не закончится "
        while isborder(r,HorizonSide((Int(side)+3)%4)) && flag
            move!(r, side)
        end
        " Вернуть в первоначальную траекторию передвижения "
        while k > 0 
            move!(r, HorizonSide((Int(side)+3)%4)) 
            k -= 1
        end
        if k==0 && !ismarker(r)
            flag = false
        end
    end
end

" Возвращение направления, противоположного заданному "
function inverse(side::HorizonSide) 
    HorizonSide(mod(Int(side)+2, 4))
end

" Передвижение в заданном направлении до тех пор, пока исполнитель находится в клетке с маркером и на пути нет стены "
function move_to_marker(r::Robot, side::HorizonSide)
    while ismarker(r) && !(isborder(r,side))
        move!(r, side)
    end
end

" Перемещение до ближайшей стены в заданном направлении "
function move_to_border(r::Robot, side::HorizonSide)
    while !(isborder(r, side))
        move!(r, side)
    end
end

" Передвижение в нижний правый угол и возвращение пути "
function move_to_corner(r::Robot)
    wayCode = []
    xInARow = 0
    yInARow = 0
    " Пока не достигнут правый нижний угол "
    while !(isborder(r, right)) || !(isborder(r, down))
        if !(isborder(r, right))
            if yInARow != 0
                " Зафиксировать координату передвижения по y "
                vectorWay = "y", string(-yInARow)
                vectorWay = join(vectorWay)
                push!(wayCode, vectorWay)
                yInARow = 0
            end

            move!(r, right)
            xInARow += 1
        else
            if xInARow != 0
                " Зафиксировать координату передвижения по x "
                vectorWay = "x", string(xInARow)
                vectorWay = join(vectorWay)
                push!(wayCode, vectorWay)
                xInARow = 0
            end

            move!(r, down)
            yInARow += 1
        end
    end

    if yInARow != 0
        " Зафиксировать координату передвижения по y "
        vectorWay = "y", string(-yInARow)
        vectorWay = join(vectorWay)
        push!(wayCode, vectorWay)
    end
    if xInARow != 0
        " Зафиксировать координату передвижения по x "
        vectorWay = "x", string(xInARow)
        vectorWay = join(vectorWay)
        push!(wayCode, vectorWay)
    end

    return (wayCode)
end

" Передвижение по заданным координатам "
function move_by_coordinates(r::Robot, wayCode)
    lenSide = 0

    for i in 1:length(wayCode)

        num = []
        way = wayCode[i]
        " Определение оси передвижения "
        axis = way[1]
        " Определение направление передвижения "
        if (way[2] != '-')
            for i in 1:length(way)-1
                push!(num, way[i+1])
            end
            num = join(num)
            lenSide = parse(Int, num)
        else
            for i in 1:length(way)-2
                push!(num, way[i+2])
            end
            num = join(num)
            lenSide = -parse(Int, num)
        end

        " Передвижение по оси x "
        if (axis == 'x')
            " Передвижение в положительную сторону оси "
            if lenSide > 0
                for i in 1:lenSide
                    move!(r, right)
                end
            else
                " Передвижение в отрицательную сторону оси " 
                lenSide = abs(lenSide)
                for i in 1:lenSide
                    move!(r, left)
                end
            end
        end

        " Передвижение по оси y "
        if (axis == 'y')
            " Передвижение в положительную сторону оси "
            if lenSide > 0
                for i in 1:lenSide
                    move!(r, up)
                end
            else 
                " Передвижение в отрицательную сторону оси "
                lenSide = abs(lenSide)
                for i in 1:lenSide
                    move!(r, down)
                end
            end
        end
    end
end

" Изменение порядка закодированного пути на конец-начало "
function inverse_wayCode(wayCode)
    " Вектор пути с инвертированными направлениями осей "
    minusWayCode = []
    " Вектор пути с инвертированными направлениями осей и порядка пути на конец-начало "
    deWayCode = []

    " Для каждой координаты пути сбор данных по направлению каждого передвижения и инверсирование численной части "
    for i in 1:length(wayCode)
        " Работа с отдельной координатой пути "
        way = wayCode[i]
        " Вектор, собирающий численную часть координаты "
        num = []

        " Определение оси движения закодированного пути "
        if way[1] == 'x'
            axis = ["x"]
        else
            axis = ["y"]
        end

        " Инверсирование численной части координаты "
        if way[2] != '-'
            push!(axis, "-")
            for j in 1:length(way)-1
                push!(num, way[j+1])
            end
        else
            for j in 1:length(way)-2
                push!(num, way[j+2])
            end
        end

        " Сборка пути, у которого инвертирована только численная часть координат"
        num = join(num)
        push!(axis, num)
        push!(minusWayCode, join(axis))
    end

    " Сборка пути, инверсированного по порядку передвижения на основе пути с инвертированной численной частью исходных координат "
    for i in -length(wayCode):-1
        push!(deWayCode, minusWayCode[-i])
    end

    return(deWayCode)
end

" Передвижение до ближайшей стены в указанном направлении и возвращение длины пути "
function move_enumerate(r::Robot, side::HorizonSide)
    pathLen = 0
    while !(isborder(r, side))
        move!(r, side)
        pathLen += 1    
    end
    if side == up
        codePath = codeCoord("y", pathLen)
    elseif side == down
        codePath = codeCoord("y", -pathLen)
    elseif side == right
        codePath = codeCoord("x", pathLen)
    elseif side == left
        codePath = codeCoord("x", -pathLen)
    end
    return pathLen, codePath
end

" Маркируемое передвижение до ближайшей стены в указанном направлении и возвращение длины пути "
function mark_enumerate(r::Robot, side::HorizonSide)
    pathLen = 0
    while !(isborder(r, side)) 
        putmarker!(r)
        move!(r, side)
        pathLen += 1    
    end
    if side == up
        codePath = codeCoord("y", pathLen)
    elseif side == down
        codePath = codeCoord("y", -pathLen)
    elseif side == right
        codePath = codeCoord("x", pathLen)
    elseif side == left
        codePath = codeCoord("x", -pathLen)
    end
    putmarker!(r)
    return pathLen, codePath
end

" Передвижение по прямому пути 'насквозь' до внешней рамки"
function move_up_to_frame(r::Robot, side::HorizonSide)
    flag = true
    while flag
        " Пока есть возможность передвижения в заданном направлении "
        while !(isborder(r, side))
            move!(r, side)
        end
        " k - счётчик передвижений вбок"
        k = 0
        " Попытка найти конец перегородки, находящейся впереди "
        while isborder(r, side) && (!isborder(r, HorizonSide((Int(side)+1)%4)))
            move!(r, HorizonSide((Int(side)+1)%4))
            k += 1
        end
        " Если удалось найти конец перегородки - встаёт сбоку от неё "
        if !isborder(r, side)
            move!(r,side)
        end
        " Если препятствием была внешняя рамка - начать прекращение работы цикла функции "
        if isborder(r,side) && isborder(r,HorizonSide((Int(side)+1)%4))
            flag = false
        end
        " Пока сбоку прямоугольная перегородка, которая мешает передвижению - идти вдоль неё, пока она не закончится "
        while isborder(r,HorizonSide((Int(side)+3)%4))
            move!(r, side)
        end
        " Вернуть в первоначальную траекторию передвижения "
        while k > 0 
            move!(r, HorizonSide((Int(side)+3)%4))
            k -= 1
        end
    end
end

" Маркировка пути 'насквозь' определённой длины "
function mark_path_lengthwise(r::Robot, side::HorizonSide, pathLen::Int64)  
    while pathLen > 1
        " Пока есть возможность передвижения в заданном направлении "
        while !(isborder(r, side)) && pathLen > 1
            move!(r, side)
            pathLen -= 1
            putmarker!(r)
        end
        " k - счётчик передвижений вбок"
        k = 0
        " Попытка найти конец перегородки, находящейся впереди "
        while isborder(r, side) && (!isborder(r, HorizonSide((Int(side)+1)%4)))
            move!(r, HorizonSide((Int(side)+1)%4))
            k += 1
        end
        " Если удалось найти конец перегородки - встаёт сбоку от неё "
        if !isborder(r, side) && k > 0 
            move!(r,side)
            pathLen -= 1
        end
        " Пока не достугнут предел счётчика сбоку прямоугольная перегородка, которая мешает передвижению - идти вдоль неё, пока она не закончится "
        while isborder(r,HorizonSide((Int(side)+3)%4)) && k > 0
            move!(r, side)
            pathLen -= 1
        end
        " Вернуть в первоначальную траекторию передвижения "
        while k > 0 
            move!(r, HorizonSide((Int(side)+3)%4))
            k -= 1
        end
        if k == 0 && pathLen >= 1
            putmarker!(r)
        end
    end
end

" Получение результирующих координат кода пути по Ox и Oy "
function getTotalNumCoord(wayCode)
    num = []
    sum = [0, 0]
    for i in 1:length(wayCode)
        way = wayCode[i]
        num = []

        " Проверка знака координаты "
        if way[2] != '-'
            for j in 1:length(way)-1
                push!(num, way[j+1])
            end
        else
            push!(num, "-")
            for j in 1:length(way)-2
                push!(num, way[j+2])
            end
        end

        num = join(num)
        " Проверка оси координаты "
        if way[1] == 'x'
            sum[1] += parse(Int, num)
        else
            sum[2] += parse(Int, num)
        end
    end

    return sum
end

" Возвращение в первоначальное положение по пути перемещения от изначальной точки в правый нижний угол "
function returnToTheBase(r::Robot, wayCode)
    move_to_corner(r)
    wayCode = inverse_wayCode(wayCode)
    move_by_coordinates(r, wayCode)
end

" Получение координаты в необходимом виде для определённых функций в данной библиотеке "
function codeCoord(typeOfAxis::String, num::Int)
    return(join(push!([typeOfAxis], string(num))))
end

" Нахождение прямоугольного препятствия, длина которого минимум 1x1, в прямоугольном поле "
function find_partition(r::Robot)
    side = right
    move_to_corner(r)
    wide = move_enumerate(r, left)[1]
    move = wide
    while move == wide && !(isborder(r, up))
        move!(r, up)
        move = move_enumerate(r, side)[1]
        side = inverse(side)
    end
end

" Маркирование пути вдоль перегородки "
function mark_along_the_border(r::Robot, sideBorder::HorizonSide, sideMove::HorizonSide)
    putmarker!(r)
    while isborder(r, sideBorder)
        move!(r, sideMove)
        putmarker!(r)
    end
end
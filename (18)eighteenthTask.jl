using HorizonSideRobots
include("roblib.jl")

" Передвижение и отметка маркером всех углов в поле, которое имеет внутренние прямоугольные перегородки "
function mark_allCorners(r::Robot)
    " Перемещение в правый нижний угол и получение пути перемещения "
    way = move_to_corner(r)

    " Перемещение в правый нижний угол и обход по периметру, с отметкой углов маркером "
    move_to_corner(r)
    for i in 0:3
        move_to_border(r, HorizonSide(i))
        putmarker!(r)
    end

    " Перемещение из правого нижнего угла в первоначальную позицию по обратному пути"
    way = inverse_wayCode(way)
    move_by_coordinates(r, way)
end
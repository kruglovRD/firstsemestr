using HorizonSideRobots
include("roblib.jl")

" Нахождение маркера в бесконечном поле, обходя всё пространство по спирали"
function findMarkerInEndlessField(r::Robot)
    flag = true
    pathLen = 1
    while flag
        for i in 0:3
            pathLenRes = pathLen
            while pathLenRes > 0 && flag
                move!(r, HorizonSide(i))
                if ismarker(r)
                    " Маркер найден "
                    flag = false
                end
                pathLenRes -= 1
            end
            pathLen += 1
        end
    end
end
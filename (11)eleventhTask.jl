using HorizonSideRobots
include("roblib.jl")

" Установка маркеров на границах внешнего поля по широте и долготе первоначальной позиции робота "
function mark_outer_border_cells(r::Robot)
    " way - код пути до угла. OxOy - перевод кода пути в результирующие координаты Ox и Oy"
    way = move_to_corner(r)
    OxOy = getTotalNumCoord(way)
    " Перейти из нижнего правого угла в правую границу широты "
    move_by_coordinates(r, [codeCoord("y", -OxOy[2])])
    putmarker!(r)
    move_to_border(r, up)
    " Перейти из верхнего правого угла в верхнюю границу долготы "
    move_by_coordinates(r, [codeCoord("x", -OxOy[1])])
    putmarker!(r)
    move_to_corner(r)
    " Перейти из нижнего правого угла в нижнюю границу долготы "
    move_by_coordinates(r, [codeCoord("x", -OxOy[1])])
    putmarker!(r)
    move_to_border(r, left)
    " Перейти из нижнего левого угла в левую границу широты "
    move_by_coordinates(r, [codeCoord("y", -OxOy[2])])
    putmarker!(r)
    " Возвращение в первоначальную позицию "
    returnToTheBase(r, way)
end
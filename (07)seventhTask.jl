using HorizonSideRobots
include("roblib.jl")

" Маркирует пустое поле в шахматном порядке таким образом, что первоначальное положение робота будет замаркировано "
function chessField(r::Robot)
    " Запоминает путь до первоначального положения "
    way = move_to_corner(r)
    " Переводит путь в координаты Ox и Oy "
    OxOy = getTotalNumCoord(way)
    " Передвигается в правый нижний угол "
    move_to_corner(r)

    " Если клетка не должна быть заштрихована, то робот передвигается в необходимую сторону "
    if mod(OxOy[1]+OxOy[2],2) == 1
        if !(isborder(r, left))
            move!(r, left)
        elseif !(isborder(r, up))
            move!(r, up)
        end
    end
    putmarker!(r)

    " side - начальное направление движения, move - счётчик движений "
    side = left
    move = 0
    " Пока не встречен верхний угол "
    while !(isborder(r, side)) || !(isborder(r, up))
        " Обход справа-налево и слева-направо, чередуется. В необходимых клетках выставляет маркер "
        while !(isborder(r, side))
            move!(r, side)
            move += 1
            " Вычисление по счётчику необходимости маркерирования клетки "
            if mod(move, 2) == 0
                putmarker!(r)
            end
        end
        " Поднимается на один 'слой' выше, чтобы пройтись по нему циклом, который находиться чуть выше"
        if !isborder(r, up)
            move!(r, up)
            move += 1
            " Вычисление по счётчику необходимости маркерирования клетки "
            if mod(move, 2) == 0
                putmarker!(r)
            end
            " Инверсия направления движения "
            side = inverse(side)
        end
    end

    " Вовзращение в первоначальную позицию "
    returnToTheBase(r, way)
end
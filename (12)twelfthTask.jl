using HorizonSideRobots

module markChess

using HorizonSideRobots
include("roblib.jl")

export markFieldLikeChess, printXY, test

" Маркировка поля шахматным видом, стороны квадратов которых задаются пользователем (n)"
function markFieldLikeChess(r, n)
    " wayCode - код пути от первоначальной позиции в правый нижний угол, X и Y координаты осей Ox (справа-налево) и Oy(снизу-вверх) "
    wayCode = move_to_corner(r)
    global X,Y = 1, 1

    " side - первоначальное направление движения"
    " Контрольный проход по первому ряду "
    side = left
    while !(isborder(r, side))
        markCell(r, n)
        move!(r, side)
        if side == left
            X += 1
        else
            X -= 1
        end
    end
    side = inverse(side)
    markCell(r, n)
    " Проход справа-налево и слева-направо до верхнего угла в конце последнего ряда "
    while (!isborder(r, up)) || (isborder(r, down) && isborder(r, left)) 
        if !(isborder(r, up))
            move!(r, up)
            Y += 1
        end
        markCell(r, n)
        while !(isborder(r, side))
            markCell(r, n)
            move!(r, side)
            if side == left
                X += 1
            else
                X -= 1
            end
        end
        side = inverse(side)
        markCell(r, n)
    end

    returnToTheBase(r, wayCode)
end

function markCell(r, n)
    " Логическая формула, определяющая по координате необходимость в установке маркера "
    if (((X % (2*n) <=n) && X % (2*n) != 0) && ((Y % (2*n) <=n)  && Y % (2*n) != 0)) || (((X % (2*n) > n) || (X % (2*n) == 0)) && ((Y % (2*n) > n) || (Y % (2*n) == 0)))
        putmarker!(r)
    end
end

end
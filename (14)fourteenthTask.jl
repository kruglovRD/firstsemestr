using HorizonSideRobots
include("roblib.jl")

" Помечает своё местоположение маркером, отмечая путь до ближайших стен по осям x и y и своего первоначального положения "
function mark_cross(r::Robot)
    " Обход по всем направлениям, отмечая путь маркером "
    for i in 0:3
        " Передвижение до внешней рамки, отмечая сквозной и достигаемый путь маркером "
        mark_up_to_frame(r, HorizonSide(i))
        move_up_to_noMarkCell(r, HorizonSide((i+2) % 4))
    end

    putmarker!(r)
end
using HorizonSideRobots
include("roblib.jl")

" Нахождение прохода в бесконечной прямой под роботом "
function findDoor(r::Robot)
    if !(isborder(r, down))
        move!(r, down)
    end
    
    " side - первоначальное направление движения. num - количество клеток, которое необходимо пройти "
    side = left
    num = 0
    while isborder(r, down)
        coord = ["x"]
        if side == left
            num = -num - 1
            side = inverse(side)
        else
            num = -num + 1
            side = inverse(side)
        end
        " Запись координат в стандартном виде, а затем подача в таком виде функции, отвечающей за передвижение по координатам "
        coord = [join(push!(coord, string(num)))]
        move_by_coordinates(r, coord)
    end
    move!(r, down)
end
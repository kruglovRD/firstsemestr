using HorizonSideRobots
include("roblib.jl")

function mark_steps(r)
    " Перемещение в правый нижний угол и получение пути перемещения "
    way = move_to_corner(r)

    " Определение необходимого для преодоления пути и помечение убывающих на одну клетку в длине рядов маркером "
    cells_to_mark = mark_enumerate(r, left)[1]
    while !(isborder(r, up)) && (cells_to_mark > 0)
        move!(r, up)
        mark_path_lengthwise(r, right, cells_to_mark)
        move_up_to_frame(r, left)
        putmarker!(r)
        cells_to_mark -= 1
    end

    " Перемещение в правый нижни угол, а затем перемещение из правого нижнего угла в первоначальную позицию по обратному пути "
    returnToTheBase(r, way)
end
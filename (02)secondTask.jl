using HorizonSideRobots
include("roblib.jl")

" Помечает периметр внешней рамки маркером и возвращается в первоначальную позицию "
function mark_perimeter(r::Robot)
    " Передвижение в нижний правый угол и получение пути перемещения "
    way = move_to_corner(r)

    " Обход и попутное маркирование периметра "
    for i in 0:3 
        mark_up_to_border(r, HorizonSide(i))
    end

    " Вовзращение в первоначальную позицию "
    returnToTheBase(r, way)
end

using HorizonSideRobots
include("roblib.jl")

" Определение средней температуры всех замаркированных клеток "
function findMarkerInField(r::Robot)
    " Передвижение в правый нижний угол "
    move_to_corner(r)
    " wide - ширина поля, side - первоначальное направление движения, flag - флаг, отвежающий за работу главного цикла, temperatureMarkers - массив из различных значений температуры замаркированных клеток "
    wide = move_enumerate(r, left)
    side = right
    flag = true
    temperatureMarkers = []
    while flag
        if ismarker(r)
            push!(temperatureMarkers, temperature(r))
        end
        " Проход слева-направо и справа-налево в поисках маркеров "
        for i in 1:wide
            move!(r, side)
            if ismarker(r)
                push!(temperatureMarkers, temperature(r))
            end
        end
        " Если достигнут верхний угол конца самого верхнего ряда поля "
        if isborder(r,side) && isborder(r, up)
            flag = false
        end
        side = inverse(side)
        " Если сверху есть ещё один не исследованный ряд - перейти на него"
        if !isborder(r, up)
            move!(r, up)
        end
    end
    " Сумма всех значений температуры в замакированных клеток, поделённая на количество значений температуры (нахождение среднего арифмитического) "
    return(sum(temperatureMarkers)/length(temperatureMarkers))
end